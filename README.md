# Compred

---

This repository comes in support for the paper ***On the Quality of Compositional Prediction for prospective Analytics on Graphs***

It is best and easier to run it using IntelliJ Idea CE (2020 or above) with scala plugin
installed.

**Beware that you will need 64Gb of ram to run this program because the dataset used for training is generous.**

Just import it as an SBT project : `File -> New -> project from existing sources`.

Edit the configuration and add the following VM option for Compred and CompredMacro : `-Xmx50g`

### Running

Every runnable scala file contains a main method, including a configuration section identified by a comment.

Configure `application.conf` the way you need and you should be good to go. 

### Dependencies :
 
Just check build.sbt for dependencies. Just know that if you do not run the project using IntelliJ Idea you may 
have to install `Spark 3.1.x` as a system dependency.

In short, dependencies are the following :

 - Smile 2.5.3 (scala ML library)
 - Spark 3.1.0 (Data preprocessing)
 - XStream 3.9.2 (ML model serialization)
 - Scala-logging 1.4.13 (logging system)
 - Config 1.4.0 (Application conf)

---

## Code


### Experiment Code
 
1. **Compred.scala** _File containing the code for the **Micro learning and compositional** learning real world experiment using anonymized data covering the period described in the paper_
2. **CompredMacro.scala** _File containing the code for the **Macro** real world experiment using anonymized data covering the period described in the paper_
3. **Mock.scala** _File containing the artificial bus network generation and experiment as described in the paper_

### Utility code
- **FilesUtils.scala** _File containing utilities to easily load files from disk_

---

## Resources

### Candidates bus lines
Meta data of the bus lines used for the experiments
 ![sample_line](bus_meta.png)

### Models
Models are provided if you do not want or cannot rebuild them.<br/>
**_N.B:  For reproducibility purposes, generating your own model is highly advised._**

### Data
1. *historical_data_anon* _Contains real world data formatted as Parquet. See the paper for more details_

```
+--------+--------+-------------------+-------------------+--------+---------------+-----------+----------+-------+---+----------+----------+-------------------+----------+---------+---------+---------+----+---------+---------+---------+-----+
|ref_orig|ref_dest|    orig_start_time|  dest_arrival_time|chaining|effective_speed|travel_time|dwell_time| period|day|order_orig|order_dest|         trip_start|incomplete|deviation|direction|      key|line|line_type|stop_orig|stop_dest|   sm|
+--------+--------+-------------------+-------------------+--------+---------------+-----------+----------+-------+---+----------+----------+-------------------+----------+---------+---------+---------+----+---------+---------+---------+-----+
|       R|       R|2019-12-13 16:40:01|2019-12-13 16:52:59|       6|           21.2|      778.0|         0|[16-17[|  4|         1|         2|2019-12-13 16:40:01|     false|    false|        A|8447-9146| 714|        4|    72835|    71007|43619|
|       N|       N|2019-12-13 16:10:14|2019-12-13 16:11:05|      14|           29.7|       51.0|         0|[16-17[|  4|         3|         4|2019-12-13 16:07:01|     false|    false|        A|7243-7721| 783|        1|    71666|    68641|36430|
|       R|       R|2019-12-13 08:50:45|2019-12-13 08:56:40|      23|           44.4|      441.0|        86|[08-09[|  4|         7|         8|2019-12-13 08:40:00|     false|    false|        R|7442-2675| 888|        4|    69216|    57528| 5020|
|       R|       T|2019-12-13 19:53:39|2019-12-13 20:01:51|      24|           32.8|      492.0|         0|[20-23[|  4|         1|         2|2019-12-13 19:43:01|     false|    false|        R|4955-3277| 889|        1|    22544|    25987|49411|
|       R|       R|2019-12-13 06:17:37|2019-12-13 06:25:40|       4|           46.5|      483.0|         0|[06-07[|  4|         1|         2|2019-12-13 06:13:01|     false|    false|        A|4997-3844| 457|        2|    54776|    72124|29269|
|     ...|     ...|                ...|                ...|     ...|            ...|        ...|       ...|    ...|...|       ...|       ...|                ...|       ...|      ...|      ...|      ...| ...|      ...|      ...|      ...|  ...|
+--------+--------+-------------------+-------------------+--------+---------------+-----------+----------+-------+---+----------+----------+-------------------+----------+---------+---------+---------+----+---------+---------+---------+-----+
```


2. *osm_anon* _Contains anonymized Open Street Map data gathered using a custom version of
   a tool we developed : `osm_bus_extractor` (see more [here](https://gitlab.inria.fr/glyan/osm_bus_extractor))_
   _See the paper for more details about this dataset_

```
+--------+-----------+------+------------------+------------+--------+------------+----------------+-------------------+-----------------------+------------------+--------------------+--------------+----+---------+---------+---------+
|chaining|legal_speed|length|nb_traffic_signals|nb_crossings|nb_stops|nb_give_ways|nb_lvl_crossings|proportion_bus_lane|proportion_bike_on_road|proportion_one_way|proportion_slow_zone|nb_roundabouts|line|stop_orig|stop_dest|direction|
+--------+-----------+------+------------------+------------+--------+------------+----------------+-------------------+-----------------------+------------------+--------------------+--------------+----+---------+---------+---------+
|      14|       50.0| 373.5|                 0|           7|       0|           1|               0|                0.0|                    1.0|               0.0|                 0.0|             0| 881|    56633|    45517|        A|
|      83|       50.0| 293.5|                 0|           7|       0|           0|               0|                0.0|                   0.23|               1.0|                 0.0|             0| 861|    48199|    56876|        A|
|      38|       49.4|3062.0|                10|          24|       0|           1|               0|               0.21|                   0.87|              0.77|                 0.0|             1| 520|    61232|    27645|        A|
|      57|       49.6| 769.0|                 5|           9|       0|           0|               0|               0.22|                   0.54|               1.0|                 0.0|             0| 811|    29240|    25556|        R|
|       1|       50.0| 164.0|                 5|           5|       0|           0|               0|               0.74|                   0.74|              0.74|                 0.0|             0| 858|    60152|    40905|        A|
|     ...|        ...|   ...|               ...|         ...|     ...|         ...|             ...|                ...|                    ...|               ...|                 ...|           ...| ...|      ...|      ...|      ...|
+--------+-----------+------+------------------+------------+--------+------------+----------------+-------------------+-----------------------+------------------+--------------------+--------------+----+---------+---------+---------+
```
3. Training data (buit using 1. and 2.)
 ![sample_line](train_data.png)

### Analysis Tools

- `Analysis.scala` A Scala file that contains code which generates precision metrics for all the experiments.
- `Charts_macro.Rmd` A R Notebook used to generate the charts for the Macro experiments's output data
- `Charts_micro_compred.Rmd` A R Notebook used to generate the charts for the micro-learning and compositional (compred) experiments's output data
- `Commercial_speed_exponential.Rmd` A R Notebook used to generate the commercial speed using the Fernandez et.al statistical function.

**_N.B: For R notebooks, a `config.yml` file is provided in the resources for spark configuration_**

### Misc

- Resources are located at the root of the project (charts, models and results used in the paper) and `src/main/resources/` (resources used to run experiments and R notebooks)
- Models are loaded and saved from the directory defined as `workDir` in `src/main/resources/application.conf`.
  Hence to use pre-trained models, make sure the `workDir` variable points to the directory models are in.
- Models are named as follows (pseudo regex) : `rf_model_{line}_{direction}(_nf)?` with `{line}` being replaced by the bus line identifier,
  `{direction}` by the line direction and `_nf` being present for models that exclude OpenStreetMap features (nf = NoFeatures)
