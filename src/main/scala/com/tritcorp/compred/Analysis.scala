package com.tritcorp.compred

import Utils._
import org.apache.spark.sql.DataFrame
import ss.implicits._
import org.apache.spark.sql.functions._

import java.util.Locale

object Analysis {
  Locale.setDefault(Locale.ENGLISH)

  // MICRO DATASETS
  val micro_urb1: Array[String] = Array("858_1_R_2620-6675_pd")
  val micro_urb2: Array[String] = Array("787_42_A_134-7434_pd")
  val micro_urb3: Array[String] = Array("785_20_R_8698-8610_pd")
  val micro_urb4: Array[String] = Array("889_5_A_1403-8312_pd")
  val micro_urb5: Array[String] = Array("689_18_A_4609-5571_pd")

  val micro_metrop1: Array[String] = Array("683_55_R_8860-1971_pd")
  val micro_metrop2: Array[String] = Array("849_72_A_5918-2464_pd")
  val micro_metrop3: Array[String] = Array("532_23_R_4955-6459_pd")
  val micro_metrop4: Array[String] = Array("969_21_R_5272-9660_pd")

  val micro_interDistr1: Array[String] = Array("696_1_A_8419-6889_pd")
  val micro_interDistr2: Array[String] = Array("581_2_R_8819-1969_pd")

  val micro_express1:Array[String] = Array("711_15_R_1940-8431_pd")
  val micro_express2:Array[String] = Array("859_10_R_2770-9477_pd")

  // COMPRED DATASETS
  val compred_urb1: Array[String] = Array("compred_858_1_R_raw")
  val compred_urb2: Array[String] = Array("compred_787_42_A_raw")
  val compred_urb3: Array[String] = Array("compred_785_20_R_raw")
  val compred_urb4: Array[String] = Array("compred_889_5_A_raw")
  val compred_urb5: Array[String] = Array("compred_689_18_A_raw")

  val compred_metrop1: Array[String] = Array("compred_683_55_R_raw")
  val compred_metrop2: Array[String] = Array("compred_849_72_A_raw")
  val compred_metrop3: Array[String] = Array("compred_532_23_R_raw")
  val compred_metrop4: Array[String] = Array("compred_969_21_R_raw")

  val compred_interDistr1: Array[String] = Array("compred_696_1_A_raw")
  val compred_interDistr2: Array[String] = Array("compred_581_2_R_raw")

  val compred_express1:Array[String] = Array("compred_711_15_R_raw")
  val compred_express2:Array[String] = Array("compred_859_10_R_raw")

  // MACRO DATASETS
  val mac_urb1: Array[String] = Array("858_1_R_macro_raw")
  val mac_urb2: Array[String] = Array("787_42_A_macro_raw")
  val mac_urb3: Array[String] = Array("785_20_R_macro_raw")
  val mac_urb4: Array[String] = Array("889_5_A_macro_raw")
  val mac_urb5: Array[String] = Array("689_18_A_macro_raw")

  val mac_metrop1: Array[String] = Array("683_55_R_macro_raw")
  val mac_metrop2: Array[String] = Array("849_72_A_macro_raw")
  val mac_metrop3: Array[String] = Array("532_23_R_macro_raw")
  val mac_metrop4: Array[String] = Array("969_21_R_macro_raw")

  val mac_interDistr1: Array[String] = Array("696_1_A_macro_raw")
  val mac_interDistr2: Array[String] = Array("581_2_R_macro_raw")

  val mac_express1:Array[String] = Array("711_15_R_macro_raw")
  val mac_express2:Array[String] = Array("859_10_R_macro_raw")

  // LINES GROUPS FOR MICRO
  val urbans_micro: Array[String] = micro_urb1 ++ micro_urb2 ++ micro_urb3 ++ micro_urb4 ++ micro_urb5
  val metrops_micro: Array[String] = micro_metrop1 ++ micro_metrop2 ++ micro_metrop3 ++ micro_metrop4
  val interDistricts_micro:Array[String] = micro_interDistr1 ++ micro_interDistr2
  val express_micro:Array[String] = micro_express1 ++ micro_express2

  val urbans_compred: Array[String] = compred_urb1 ++ compred_urb2 ++ compred_urb3 ++ compred_urb4 ++ compred_urb5
  val metrops_compred: Array[String] = compred_metrop1 ++ compred_metrop2 ++ compred_metrop3 ++ compred_metrop4
  val interDistricts_compred:Array[String] = compred_interDistr1 ++ compred_interDistr2
  val express_compred:Array[String] = compred_express1 ++ compred_express2

  val urbans_macro: Array[String] = mac_urb1 ++ mac_urb2 ++ mac_urb3 ++ mac_urb4 ++ mac_urb5
  val metrops_macro: Array[String] = mac_metrop1 ++ mac_metrop2 ++ mac_metrop3 ++ mac_metrop4
  val interDistricts_macro:Array[String] = mac_interDistr1 ++ mac_interDistr2
  val express_macro:Array[String] = mac_express1 ++ mac_express2

  val all_micro: Array[String] = urbans_micro ++ metrops_micro ++ express_micro++ interDistricts_micro
  val all_compred: Array[String] = urbans_compred ++ metrops_compred ++express_compred ++ interDistricts_compred
  val all_macro: Array[String] = urbans_macro ++ metrops_macro ++ express_macro ++ interDistricts_macro

  def load_data(dsNames: Array[String], nf: Boolean): DataFrame = {
    val ext = if (nf) "_nf" else ""
    val dfs = dsNames.map { n =>
      val ds = n+ext
      val line = n.replace("compred_","").split("_").head
      try {
        println(s"LOADING $ds")
        Some(
          ss.read.option("header", "true").csv(s"$workDir/OUT/$ds").withColumn("line",lit(line))
        )
      }
      catch {
        case e: Throwable => println(s"NO DATA FOR : $n$ext")
          None
      }
    }.filter(_.nonEmpty)
    if (dfs.nonEmpty)
      dfs.map(_.get).reduce((df, df1) => df.union(df1))
    else
      throw new Exception("NO DATA TO LOAD")
  }

  def main(args: Array[String]) {
    /** ***************************************************************************************************************
      * CONFIGURATION
      * ************************************************************************************************************** */

    val micro_ml_data = all_micro//all_micro urbans_micro metrops_micro interDistricts_micro express_micro
    val compositional_ml_data = all_compred//all_compred urbans_compred metrops_compred interDistricts_compred express_compred
    val macro_ml_data = all_macro//all_macro urbans_macro metrops_macro interDistricts_macro express_macro
    val nf: Boolean = false

    /** ***************************************************************************************************************
      * START OF APP CODE
      * ************************************************************************************************************** */

    val micro = load_data(micro_ml_data, nf)
    //.where($"holidays" === 6 and $"day" === 0)
    val microKnown = micro.where($"known" === true)
    val microUnknown = micro.where($"known" === false)

    var n1 = 1.0 / microKnown.count
    println("-----------------------------------------------------------------------------------------------------------------\n")
    println("--- MICRO ---")
    println("-----------------------------------------------------------------------------------------------------------------")
    println("----------------------")
    println("--- MICRO KNOWN ---")
    println("----------------------")
    println("MICRO KNOWN MAE : ")
    microKnown.withColumn("err", abs($"pred" - $"truth")).agg(avg("err")).show
    println(s"""MICRO KNOWN RMSE : ${Math.sqrt(n1 * microKnown.withColumn("err", ($"truth" - $"pred") * ($"truth" - $"pred")).agg(sum("err")).collect.head.getAs[Double](0))}""")
    println(s"""MICRO KNOWN MAPE : ${n1 * microKnown.withColumn("err", abs(($"truth" - $"pred") / ($"truth"))).agg(sum("err")).collect.head.getAs[Double](0)}""")
    println("----------------------")
    println("--- MICRO UNKNOWN ---")
    println("----------------------")
    n1 = 1.0 / microUnknown.count
    println("MICRO UNKOWN MAE : ")
    microUnknown.withColumn("err", abs($"pred" - $"truth")).agg(avg("err")).show
    println(s"""MICRO UNKOWN RMSE : ${Math.sqrt(n1 * microUnknown.withColumn("err", ($"truth" - $"pred") * ($"truth" - $"pred")).agg(sum("err")).collect.head.getAs[Double](0))}""")
    println(s"""MICRO UNKOWN MAPE : ${n1 * microUnknown.withColumn("err", abs(($"truth" - $"pred") / ($"truth"))).agg(sum("err")).collect.head.getAs[Double](0)}""")
    println("----------------------")
    println("--- MICRO GLOBAL ---")
    println("----------------------")
    n1 = 1.0 / micro.count
    println("MICRO MAE : ")
    micro.withColumn("err", abs($"pred" - $"truth")).agg(avg("err")).show
    println(s"""MICRO RMSE : ${Math.sqrt(n1 * micro.withColumn("err", ($"truth" - $"pred") * ($"truth" - $"pred")).agg(sum("err")).collect.head.getAs[Double](0))}""")
    println(s"""MICRO MAPE : ${n1 * micro.withColumn("err", abs(($"truth" - $"pred") / ($"truth"))).agg(sum("err")).collect.head.getAs[Double](0)}""")

    println(s"""MICRO PREDICTION STD DEV (grouped by period and line)""")
    micro.groupBy("period","pd","line").agg(stddev_pop($"pred").as("sd")).agg(avg($"sd")).show

    println("-----------------------------------------------------------------------------------------------------------------\n")
    println("--- COMPRED ---")
    println("-----------------------------------------------------------------------------------------------------------------")
    val compred = load_data(compositional_ml_data, nf)
    //.where($"holidays" === 6 and $"day" === 0)
    val compredTrips = compred.count
    n1 = 1.0 / compredTrips
    println(s"Number of trips: $compredTrips")
    println("COMPRED MAE : ")
    compred.withColumn("err", abs($"pred_speed" - $"true_speed")).agg(avg("err")).show
    println(s"""COMPRED RMSE : ${Math.sqrt(n1 * compred.withColumn("err", ($"true_speed" - $"pred_speed") * ($"true_speed" - $"pred_speed")).agg(sum("err")).collect.head.getAs[Double](0))}""")

    println(s"""COMPRED MAPE : ${n1 * compred.withColumn("err", abs(($"true_speed" - $"pred_speed") / ($"true_speed"))).agg(sum("err")).collect.head.getAs[Double](0)}""")

    println("-----------------------------------------------------------------------------------------------------------------\n")
    println("--- MACRO ---")
    println("-----------------------------------------------------------------------------------------------------------------")
    val mac = load_data(macro_ml_data, nf)
    val macTrips = mac.count
    //.where($"holidays" === 6 and $"day" === 0)

    n1 = 1.0 / macTrips
    println(s"Number of trips: $macTrips")
    println("MACRO MAE : ")
    mac.agg(avg("rmse")).show
    println(s"""MACRO RMSE : ${Math.sqrt(n1 * mac.withColumn("err", ($"true_speed" - $"pred_speed") * ($"true_speed" - $"pred_speed")).agg(sum("err")).collect.head.getAs[Double](0))}""")
    println(s"""MACRO MAPE : ${n1 * mac.withColumn("err", abs(($"true_speed" - $"pred_speed") / ($"true_speed"))).agg(sum("err")).collect.head.getAs[Double](0)}""")

    compred.select("true_speed", "pred_speed", "period", "holidays", "day","line")
      .union(mac.select("true_speed", "pred_speed", "period", "holidays", "day","line"))
      .groupBy("period", "holidays", "day","line")
      .agg(avg("true_speed").as("true_speed"), avg("pred_speed").as("pred_speed"))
      .withColumn("pd", num2Period($"period")).coalesce(1)
      .write.option("header", "true").mode("overwrite")
      .csv(s"$workDir/OUT/averaged")

    println(s"""COMPRED/MACRO PREDICTION STD DEV (grouped by period and line)""")
    compred.groupBy("period","pd","line").agg(stddev_pop($"pred_speed").as("sd")).orderBy("line","pd").agg(avg("sd").as("avg_compred_prediction_variation")).show
    mac.groupBy("period","pd","line").agg(stddev_pop($"pred_speed").as("sd")).orderBy("line","pd").agg(avg("sd").as("avg_macro_prediction_variation")).show

    /** NOT USED IN THE PAPER.
      * Note that we observed that compositional prediction tends to overestimate speed while macro prediction tends to underestimate speed.
      * A reconciliation method could be to consider a threshold based on path's length over which both models should be used so that the average of each prediction yields better precision results
      * than the using of a single model.
      * However, the validity and genericity of such a method have yet to be confirmed
      *
    println("-----------------------------------------------------------------------------------------------------------------\n")
    println("--- AVG(COMPRED+MACRO) ---")
    println("-----------------------------------------------------------------------------------------------------------------")
    val av = ss.read.option("header", "true").csv(s"$workDir/OUT/averaged")
      //.where($"holidays" === 6 and $"day" === 0)

    n1 = 1.0 / av.count
    println("AVG MAE : ")
    av.withColumn("err", abs($"pred_speed" - $"true_speed")).agg(avg("err")).show
    println(s"""AVG RMSE : ${Math.sqrt(n1 * av.withColumn("err", ($"true_speed" - $"pred_speed") * ($"true_speed" - $"pred_speed")).agg(sum("err")).collect.head.getAs[Double](0))}""")

    println(s"""AVG MAPE : ${n1 * av.withColumn("err", abs(($"true_speed" - $"pred_speed") / ($"true_speed"))).agg(sum("err")).collect.head.getAs[Double](0)}""")
    println("-----------------------------------------------------------------------------------------------------------------")
    */
    }
}