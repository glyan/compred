package com.tritcorp.compred

import com.tritcorp.compred.FilesUtils._
import com.tritcorp.compred.Utils._
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, functions}
import smile.data.formula.Formula
import smile.read.parquet
import smile.validation.{mad, rmse}

import java.io.File
import java.util.Locale

object CompredMacro extends LazyLogging {

  Locale.setDefault(Locale.ENGLISH)

  import ss.implicits._


  def enrich(ips: DataFrame, mac: DataFrame, lines: DataFrame): DataFrame = {
    val starts = ips.where($"order_orig" === 1 and $"orig_start_time" =!= "")
      .drop("ref_dest", "stop_dest", "order_dest", "dest_arrival_time", "dist_tot")

    val lineEnd = ips.where($"ref_dest" === "T" and $"order_dest" > 1 and $"dest_arrival_time" =!= "")
      .drop("order_orig", "ref_orig", "stop_orig", "orig_start_time")

    val data = starts.join(lineEnd.drop("period", "day"), Seq("trip_start", "line", "chaining", "direction", "sm", "line_type"))
      .join(lines, Seq("line", "chaining", "direction")).withColumn("travelTime", travelTimeCalc(col("orig_start_time"), col("dest_arrival_time")))
      .withColumn("real_speed", speedCalcFloat($"length", col("travelTime"), lit("-1.0")))
      .withColumn("pd", period2Num($"period")).withColumn("holidays", holidays($"trip_start"))
      .withColumn("period", period2Num($"period"))
      .selectExpr("period", "holidays", "day", "travelTime", "real_speed", "line", "chaining", "direction", "line_type")

    val ds = data.join(mac, Seq("line", "chaining", "direction"))
      .select("line", "chaining", "direction", "line_type", "legal_speed", "length", "nb_traffic_signals", "nb_crossings", "nb_stops", "nb_give_ways",
        "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone",
        "nb_roundabouts", "real_speed", "period", "day", "holidays")

    ds
  }


  def main(args: Array[String]): Unit = {

    /** ***************************************************************************************************************
      * CONFIGURATION
      * ************************************************************************************************************** */

    val line = "969"
    val chaining = "21"
    val direction = "R"
    val points = 11
    val remove_osm_features = true


    /** ***************************************************************************************************************
      * START OF APP CODE
      * ************************************************************************************************************** */


    //Used for outputs naming. Do not touch that
    val suffix = if (remove_osm_features) "_nf" else ""
    val mac = osm_enriched
      .withColumn("ms", $"legal_speed" / 3.6)
      .withColumn("tt", $"length" / $"ms")
      .groupBy("line", "chaining", "direction")
      .agg(
        functions.sum("tt").as("tt"),
        functions.round(functions.avg("legal_speed"), 1).as("legal_speed"),
        functions.sum("length").as("length"),
        functions.sum($"nb_traffic_signals").as("nb_traffic_signals"), functions.sum($"nb_crossings").as("nb_crossings"), functions.sum($"nb_stops").as("nb_stops"),
        functions.sum($"nb_give_ways").as("nb_give_ways"), functions.sum($"nb_lvl_crossings").as("nb_lvl_crossings"),
        functions.round(functions.avg("proportion_bus_lane"), 2).as("proportion_bus_lane"),
        functions.round(functions.avg("proportion_bike_on_road"), 2).as("proportion_bike_on_road"),
        functions.round(functions.avg("proportion_one_way"), 2).as("proportion_one_way"),
        functions.round(functions.avg("proportion_slow_zone"), 2).as("proportion_slow_zone"),
        functions.sum($"nb_roundabouts").as("nb_roundabouts")
      )
      .withColumn("legal_speed", functions.round(($"length" / $"tt") * 3.6, 1))
      .drop("tt", "ms")

    logger.warn("\n---------------------------------------------------------")
    logger.warn(s"CURRENT CONFIG : ")
    logger.warn(s"line: $line")
    logger.warn(s"direction: $direction")
    logger.warn(s"chaining: $chaining")
    logger.warn(s"number of inter-stops of the line: $points")
    logger.warn(s"dropping OSM features: $remove_osm_features")
    logger.warn("\n---------------------------------------------------------\n\n\n")

    logger.warn("Preprocessing data...")


    val lines = mac.selectExpr("line", "chaining ", "direction", "length").distinct
    val ips = historical.where($"incomplete" === false and $"deviation" === false and $"effective_speed" > 0 and $"effective_speed" < 55 and
      $"travel_time" > 0 and $"dwell_time" >= 0)

    val trainDS = enrich(ips.where($"line" =!= line), mac, lines)

    val test_trip = historical.where($"line" === line and $"chaining" === chaining and $"direction" === direction and
      $"effective_speed" < 55 and $"deviation" === false and $"incomplete" === false and $"travel_time" > 0 and $"dwell_time" >= 0)

    val trips_data = test_trip.selectExpr("stop_orig", "stop_dest ", "line",
      "chaining", "effective_speed ", "travel_time", "dwell_time", "period", "day", "order_orig", "trip_start", "sm", "incomplete")
      .orderBy($"trip_start", $"sm", $"order_orig")

    val valid_trips = trips_data.groupBy("sm", "trip_start", "period").agg(count("*").as("tot")).where($"tot" === points).distinct.drop("tot")

    val testDS = enrich(test_trip.join(valid_trips, Seq("sm", "trip_start", "period")), mac, lines)
    trainDS.coalesce(1).write.mode("overwrite").format("org.apache.spark.sql.parquet").save(s"$workDir/train_macro")


    val holidays = Array(1, 2, 3, 4, 5, 6)
    val days = Array(0, 1, 2, 3, 4, 5, 6)

    /** TRAINING MODELS
      * */

    logger.warn("TRAINING MODEL")
    var f = new File(s"$workDir/train_macro")
    var tree = getFileTree(f)
    val train = parquet(getAllFilesEndingWith(tree, ".parquet").head.getPath)


    /** RANDOM FOREST REGRESSOR */
    val xreg = if (!remove_osm_features) {
      train.drop("line", "chaining", "direction")
    } else {
      train.drop("line", "chaining", "direction", "legal_speed", "length", "nb_traffic_signals", "nb_crossings",
        "nb_stops", "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way",
        "proportion_slow_zone", "nb_roundabouts")
    }

    // GRID SEARCH
/*
        val results:ListBuffer[(String,Double,Double)] = ListBuffer[(String,Double,Double)]()

        val hp = new Hyperparameters()
          .add("smile.random.forest.trees", 50, 750, 25) // a fixed value
          .add("smile.random.forest.mtry", Array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)) // an array of values to choose
          .add("smile.random.forest.max.nodes", 100, 500, 50) // range [100, 500] with step 50
          .add("smile.random.forest.max.depth", 2, 30, 2)

        val train1 = xreg.slice(0, (xreg.size() * 0.8).toInt)
        val test1 = xreg.slice((xreg.size() * 0.8).toInt + 1, xreg.size)
        val formula = Formula.lhs("real_speed")
        val testy = formula.y(test1).toDoubleArray()

        hp.grid().parallel().forEach{prop =>
          val model = RandomForest.fit(formula, train1, prop)
          val pred = model.predict(test1)
          results.append((prop.toString,model.error(),rmse(testy, pred)))
    /*      println(prop)
          println("OOB RMSE = %.2f".format(model.error()))
          println("RMSE = %.2f".format(rmse(testy, pred)))*/
        }

        println(results.toList.minBy(_._3))
        println(results.toList.minBy(_._2))
*/
    // Default random forest configuration delivers better results that the configuration found using grid-search
 // ({smile.random.forest.mtry=13, smile.random.forest.trees=175, smile.random.forest.max.depth=30, smile.random.forest.max.nodes=500},2.337378647037799,2.375870568005383)
    val rf = smile.regression.randomForest(Formula.lhs("real_speed"), xreg /*,ntrees = 175, maxDepth = 30 ,maxNodes = 500, mtry = 13*/)
    logger.warn(s"Ouf of bag RMSE: ${rf.error()}")
    logger.warn(s"Features importance: ")
    logger.warn("___")
    logger.warn(xreg.drop("real_speed").names.zip(rf.importance).sortBy(_._2).mkString("\n"))
    logger.warn("___\n")
    logger.warn(s"WORKING ON LINE $line CHAINEING $chaining DIRECTION $direction\n")

    holidays.foreach { holiday =>
      days.foreach { day =>
        Periods.foreach { period =>

          try {
            logger.warn(s"HOLIDAYS $holiday DAY $day PERIOD $period\n")

            testDS
              .where($"day" === day and $"holidays" === holiday and $"period" === period)
              .coalesce(1).write.mode("overwrite")
              .format("org.apache.spark.sql.parquet")
              .save(s"$workDir/test_macro")


            f = new File(s"$workDir/test_macro")
            tree = getFileTree(f)
            val tests_base = parquet(getAllFilesEndingWith(tree, ".parquet").head.getPath)
            val tests = if (!remove_osm_features) {
              tests_base.drop("line", "chaining", "direction")
            } else {
              tests_base.drop("line", "chaining", "direction", "legal_speed", "length", "nb_traffic_signals", "nb_crossings",
                "nb_stops", "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way",
                "proportion_slow_zone", "nb_roundabouts")
            }

            logger.warn(s"train dataset size : ${train.size()}")
            logger.warn(s"test dataset size : ${tests.size()}")

            val preds_reg = tests_base("real_speed")

            val results = rf.predict(tests)
            val rmse_res = rmse(results, preds_reg.toDoubleArray)
            val mae = mad(results, preds_reg.toDoubleArray)
            logger.warn(s"RMSE TEST: $rmse_res")
            logger.warn(s"MAE TEST: $mae")


            val anal = preds_reg.toDoubleArray()
              .zip(results)
              .map { x => (x._1, x._2) }.toSeq.toDF("true_speed", "pred_speed")
              .withColumn("rmse", lit(rmse_res))
              .withColumn("mae", lit(mae))
              .cache

            logger.warn(s"known in DS ${anal.count}")

            anal
              .coalesce(1).write.mode("overwrite").option("header", "true")
              .format("com.databricks.spark.csv")
              .save(s"""$workDir/OUT/${line}_${chaining}_${direction}_macro_raw${suffix}/holidays=${holiday}/day=${day}/period=${period}/pd=${NumPeriod(period).replace("[", "")}""")


          }
          catch {
            case e: AssertionError => {
              logger.error("[ASSERTION ERROR]")
              logger.error(s"FOR HOLIDAYS $holiday DAY $day PERIOD ${period} - ${NumPeriod(period)}")
              logger.error(s"NO TRIPS DATA / INCOMPLETE TRIPS / COMPOSITION IMPOSSIBLE\n")

            }
            case e: Exception => {
              logger.error("[EXCEPTION]")
              logger.error(s"CANNOT DO STUFF FOR HOLIDAYS $holiday DAY $day PERIOD ${period} - ${NumPeriod(period)}\n\n")
              logger.error(s"REASON : Probably no data for the given time period\n")
            }
          }

        }
      }
    }

    val macro_raw = ss.read.option("header", "true").format("com.databricks.spark.csv")
      .load(s"""$workDir/OUT/${line}_${chaining}_${direction}_macro_raw${suffix}/""")

    macro_raw
      .groupBy("holidays", "day", "period", "pd")
      .agg(functions.avg("true_speed").as("true_speed"), functions.avg("pred_speed").as("pred_speed"))
      .coalesce(1).write.mode("overwrite").option("header", "true")
      .format("com.databricks.spark.csv")
      .save(s"""$workDir/OUT/${line}_${chaining}_${direction}_macro${suffix}""")


    ss.stop()

    logger.warn("DONE\n")
    logger.warn(s"Everything was saved @ $workDir/OUT/\n")
    logger.warn(s"Macro learning Raw (useful for stats): $workDir/OUT/${line}_${chaining}_${direction}_macro_raw${suffix}")
    logger.warn(s"Macro learning, averaged for each inter-stop (Useful for charts): $workDir/OUT/${line}_${chaining}_${direction}_macro${suffix}")

  }


}
